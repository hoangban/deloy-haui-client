import config from "../app.config"
import Axios from "axios"

export var userService;
export default ( userService = {
    login
});

async function login(username, password,remember_me) {
    const formData = new FormData();
    formData.append('username', username);
    formData.append('password', password);
    formData.append('remember_me',remember_me)
    try {
        const response = await Axios.post(config.API + 'login', formData);
        return response.data.Data;
    } catch (error) {
        return error;
    }
}
