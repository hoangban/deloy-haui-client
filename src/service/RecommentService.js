import config from "../app.config"
import { authHeader } from "../helper"
import Axios from "axios"

export var RecommentService;
export default (RecommentService = {
    // getListRecomment,
    postRecomment
});
// async function getListRecomment(limit, page) {
    // try {
        // const params = {
            // page: page,
            // limit: limit
        // };
        // const response = await Axios.get(config.API + 'recomment/list', {
            // headers: authHeader(),
            // params
        // });
        // return response.data;
    // } catch (error) {
        // return error;
    // }
// }
async function postRecomment(user_id, name) {
    try {
        console.log(user_id)
        var postData = {
            user_id: user_id,
            name: name
        };
        let axiosConfig = {
            headers: authHeader()
        };
        const response = await Axios.post(config.API + 'recomment/add', postData, axiosConfig);
        console.log(response)
    } catch (error) {
        return error;
    }
}
