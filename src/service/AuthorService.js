import config from "../app.config"
import { authHeader } from "../helper"
import Axios from "axios"

export var AuthorService;
export default ( AuthorService = {
    getListAuthor,
    postAuthor
});

async function getListAuthor(limit,page) {
    try {
        const params = {
            page: page,
            limit: limit
        };
        const response = await Axios.get(config.API + 'author/list', {
            headers: authHeader(),
            params
        });
        return response.data;
    } catch (error) {
        return error;
    }
}
async function postAuthor(name) {
    try {
        console.log(name)
        var postData = {
            name: name
        };
        //let user = JSON.parse(localStorage.getItem('user'));
        let axiosConfig = {
            headers: authHeader()
        };      
        const response = await Axios.post(config.API + 'author/add', postData, axiosConfig);
        console.log(response)
        //return response.data;
    } catch (error) {
        return error;
    }
}
