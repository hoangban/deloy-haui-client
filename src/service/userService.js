import config from "../app.config"
import { authHeader } from "../helper"
import Axios from "axios"

export var userService;
export default ( userService = {
    getListUser,
    updateUser,
    getDetailUser
});

function getListUser() {
    return Axios.get(`${config.API_URL}/users`)
        .then(response => {
            return response.data.data;
        })
        .catch( error => {
            error = error.response;
            const message = (error && error.data && error.data.message) || error.statusText;
            return message;
        });
}
function getDetailUser() {
    try {
        let user = JSON.parse(localStorage.getItem('user'));
        const response =  Axios.get(config.API + 'user/detail/'+user.id, {
            headers: authHeader(),
        });
        return response;
    } catch (error) {
        return error;
    }
}
function updateUser(data){
    try {
        var postData = {
                school : data.school,
                faculty_id : data.faculty_id,
                majors_id : data.majors_id,
                tel : data.tel,
                birthday : data.birthday,
                firstname : data.firstname,
                lastname : data.lastname,
                avatar : ""
            };
        let user = JSON.parse(localStorage.getItem('user'));
        const params = {
            id: user.id,
        };     
        let axiosConfig = {
            headers: authHeader(),
            params
        };      
        const response =  Axios.post(config.API + 'user/edit', postData, axiosConfig);
        return response;
    } catch (error) {
        return error;
    }
}