import config from "../app.config"
import { authHeader } from "../helper"
import Axios from "axios"

export var MajorService;
export default ( MajorService = {
    getListMajor,
     postMajor
});
async function getListMajor(limit,page) {
    try {
        const params = {
            page: page,
            limit: limit
        };
        const response = await Axios.get(config.API + 'major/list', {
            headers: authHeader(),
            params
        });
        return response.data;
    } catch (error) {
        return error;
    }
}
async function postMajor(name,code) {
    try {
        console.log(name)
        var postData = {
            name: name,
            code: code
        };      
        let axiosConfig = {
            headers: authHeader()
        };      
        const response = await Axios.post(config.API + 'major/add', postData, axiosConfig);
        console.log(response)
    } catch (error) {
        return error;
    }
}
