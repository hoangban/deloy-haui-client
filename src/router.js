import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '*',
            component: () => import('./components/auth/DangNhap.vue'),
        },
        {
            path: '/login',
            name: 'DangNhap',
            component: () => import('./components/auth/DangNhap.vue'),
        },
        {
            path: '/logout',
            name: 'Logout',
            component: () => import('./components/auth/Logout.vue'),
        },
        // Backend
        {
            path: '/admin',
            component: () => import('./components/backend/Master.vue'),
            meta: {
                title: 'Backend',
                backendAuth: true,
            },
            children: [
                {
                    path: '/',
                    name: 'Backend',
                    component: () => import('./components/backend/pages/Dashboard.vue'),
                    meta: {
                        title: 'Backend',
                        backendAuth: true,
                    }
                },
                {
                    path: 'document',
                    name: 'Document',
                    component: () => import('./components/backend/document/Document.vue'),
                    meta: {
                        title: 'Document',
                        backendAuth: true,
                    },
                    children: [
                        {
                            path: '/',
                            name: 'DocumentList',
                            component: () => import('./components/backend/document/List.vue'),
                            meta: {
                                title: 'DocumentList',
                                backendAuth: true,
                            }
                        },
                        {
                            path: 'create',
                            name: 'DocumentCreate',
                            component: () => import('./components/backend/document/Create.vue'),
                            meta: {
                                title: 'DocumentCreate',
                                backendAuth: true,
                            }
                        },
                        {
                            path: 'detail',
                            name: 'DocumentDetail',
                            component: () => import('./components/backend/document/Detail.vue'),
                            meta: {
                                title: 'DocumentDetail',
                                backendAuth: true,
                            }
                        },
                        {
                            path: 'cat',
                            name: 'CategoryList',
                            component: () => import('./components/backend/document/CategoryList.vue'),
                            meta: {
                                title: 'CategoryList',
                                backendAuth: true,
                            }
                        },
                        {
                            path: 'type',
                            name: 'TypeList',
                            component: () => import('./components/backend/document/TypeList.vue'),
                            meta: {
                                title: 'TypeList',
                                backendAuth: true,
                            }
                        },
                        {
                            path: 'author',
                            name: 'AuthorList',
                            component: () => import('./components/backend/document/AuthorList.vue'),
                            meta: {
                                title: 'AuthorList',
                                backendAuth: true,
                            }
                        }
                    ]
                },
                {
                    path: 'user',
                    name: 'User',
                    component: () => import('./components/backend/user/User.vue'),
                    children: [
                        {
                            path: '/',
                            name: 'UserList',
                            component: () => import('./components/backend/user/List.vue'),
                            meta: {
                                title: 'UserList',
                                requiresAuth: true,
                            }
                        },
                        {
                            path: 'create',
                            name: 'UserCreate',
                            component: () => import('./components/backend/user/Create.vue'),
                            meta: {
                                title: 'UserCreate',
                                requiresAuth: true,
                            }
                        },
                        {
                            path: 'detail/:id',
                            name: 'UserDetail',
                            component: () => import('./components/backend/user/Detail.vue'),
                            meta: {
                                title: 'UserDetail',
                                requiresAuth: true,
                            }
                        },
                        {
                            path: 'type',
                            name: 'UserType',
                            component: () => import('./components/backend/user/UserType.vue'),
                            meta: {
                                title: 'UserType',
                                requiresAuth: true,
                            }
                        },
                        {
                            path: 'major',
                            name: 'UserMajor',
                            component: () => import('./components/backend/user/UserMajor.vue'),
                            meta: {
                                title: 'UserMajor',
                                requiresAuth: true,
                            }
                        },
                        {
                            path: 'faculty',
                            name: 'UserFaculty',
                            component: () => import('./components/backend/user/UserFaculty.vue'),
                            meta: {
                                title: 'UserFaculty',
                                requiresAuth: true,
                            }
                        },
                        {
                            path: 'recomment',
                            name: 'UserRecomment',
                            component: () => import('./components/backend/user/UserRecomment.vue'),
                            meta: {
                                title: 'UserRecomment',
                                requiresAuth: true,
                            }
                        },
                    ]
                },
                {
                    path: 'borrow',
                    name: 'Borrow',
                    component: () => import('./components/backend/borrow/Borrow.vue'),
                    children: [
                        {
                            path: '/',
                            name: 'BorrowList',
                            component: () => import('./components/backend/borrow/List.vue'),
                            meta: {
                                title: 'BorrowList',
                                requiresAuth: true,
                            }
                        }
                    ]
                },
                {
                    path: 'payment',
                    name: 'Payment',
                    component: () => import('./components/backend/payment/Payment.vue'),
                    children: [
                        {
                            path: '/',
                            name: 'PaymentList',
                            component: () => import('./components/backend/payment/List.vue'),
                            meta: {
                                title: 'PaymentList',
                                requiresAuth: true,
                            }
                        }
                    ]
                },
                {
                    path: 'group',
                    name: 'Group',
                    component: () => import('./components/backend/group/Group.vue'),
                    children: [
                        {
                            path: '/',
                            name: 'GroupList',
                            component: () => import('./components/backend/group/List.vue'),
                            meta: {
                                title: 'GroupList',
                                requiresAuth: true,
                            }
                        }
                    ]
                },
                {
                    path: 'report',
                    name: 'Report',
                    component: () => import('./components/backend/report/Report.vue'),
                    children: [
                        {
                            path: '/',
                            name: 'ReportList',
                            component: () => import('./components/backend/group/List.vue'),
                            meta: {
                                title: 'ReportList',
                                requiresAuth: true,
                            }
                        }
                    ]
                },
                {
                    path: 'setting',
                    name: 'Setting',
                    component: () => import('./components/backend/setting/Setting.vue'),
                    children: [
                        {
                            path: '/',
                            name: 'SettingList',
                            component: () => import('./components/backend/setting/List.vue'),
                            meta: {
                                title: 'SettingList',
                                requiresAuth: true,
                            }
                        }
                    ]
                },
            ]
        },
        // Frontend
        {
            path: '',
            component: () => import('./components/frontend/Layout.vue'),
            children: [
                {
                    path: '/',
                    name: 'Dashboard',
                    component: () => import('./components/frontend/pages/Dashboard.vue'),
                    meta: {
                        title: 'Home',
                        requiresAuth: true,
                    }
                },
                {
                    path: 'search',
                    name: 'Search',
                    component: () => import('./components/frontend/pages/Search.vue'),
                    meta: {
                        title: 'Search',
                        requiresAuth: true,
                    }
                },
                {
                    path: 'document/:id',
                    name: 'FeDocumentDetail',
                    component: () => import('./components/frontend/document/Detail.vue'),
                    meta: {
                      title: 'DocumentDetail',
                      requiresAuth: true
                    }
                },
                {
                    path: 'document/video/:id',
                    name: 'DocumentVideo',
                    component: () => import('./components/frontend/document/DetailVideo.vue'),
                    meta: {
                      title: 'DocumentVideo',
                      requiresAuth: true
                    }
                },
                {
                    path: 'document/book/:id',
                    name: 'DocumentBook',
                    component: () => import('./components/frontend/document/DetailBook.vue'),
                    meta: {
                      title: 'DocumentBook',
                      requiresAuth: true
                    }
                },
                {
                    path: 'mypage',
                    name: 'MyPage',
                    component: () => import('./components/frontend/pages/MyPage.vue'),
                    meta: {
                      title: 'my-page',
                      requiresAuth: true
                    }
                },
                {
                    path: 'group',
                    name: 'Group',
                    component: () => import('./components/frontend/group/Group.vue'),
                    children: [
                        {
                            path: '/',
                            name: 'GroupList',
                            component: () => import('./components/frontend/group/List.vue'),
                            meta: {
                                title: 'GroupList',
                                requiresAuth: true,
                            }
                        },
                        {
                            path: 'create',
                            name: 'GroupCreate',
                            component: () => import('./components/frontend/group/Create.vue'),
                            meta: {
                                title: 'GroupCreate',
                                requiresAuth: true,
                            }
                        },
                        {
                            path: 'detail',
                            name: 'GroupDetail',
                            component: () => import('./components/frontend/group/Detail.vue'),
                            meta: {
                                title: 'GroupDetail',
                                requiresAuth: true,
                            }
                        },
                    ]
                },
            ]
        }
    ]
})

router.beforeEach((to, from, next) => {
    const user = JSON.parse(localStorage.getItem('user'))
    var today = new Date();
    if(user != null && (Date.parse(user.expires_at) < Date.parse(today.toISOString()))) {
        localStorage.removeItem('user');
        next({
            path: '/login',
            params: { nextUrl: to.fullPath }
        })
    }
    if (to.matched.some(record => record.meta.backendAuth)) {
        if (user == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            if(user.role_id == 1) {
                next()
            } else {
                next({name: 'Dashboard'}) 
            }
        }
    } else if (to.matched.some(record => record.meta.requiresAuth)) {
        if (user == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            next()
        }
    } else {
      next()
    }
  })

export default router