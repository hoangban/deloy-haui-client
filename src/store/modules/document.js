import shop from '../../service/DocumentService'

// initial state
const state = () => ({
  all: []
})

// getters
const getters = {}

// actions
const actions = {
  getDocument ({ commit }) {
    shop.getDocument(products => {
      commit('setDocument',products)
    })
  }
}

// mutations
const mutations = {
  setDocument (state, products) {
    state.all = products
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
