import MajorService from "../../service/MajorService"

const state = ()=> ({
    majors : [],
    total :''
})
const getters = {}

const actions = {
    async getAllMajor({commit},data) {
        const majors = await MajorService.getListMajor(data.perPage,data.currentPage);
        if(majors) commit('setMajor',majors)
    },
    async addMajor({commit}, data) {
        const majors = await MajorService.postMajor(data.name,data.code);
        commit('addUser',majors)
    }
}
const mutations = {
    setMajor(state,majors) {
        if(majors) {
            state.majors = majors.data
            state.total = majors.total
        }
    },
    addUser(state,majors) {
        if(majors) {
            state.majors.push(majors)
            state.total = this.total + 1
        } 
    }
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}