import AuthorService from "../../service/AuthorService"

const state = ()=> ({
    authors : [],
    total :''
})

const getters = {}

const actions = {
    async getAllAuthor({commit},data) {
        const authors = await AuthorService.getListAuthor(data.perPage,data.currentPage);
        if(authors) commit('setAuthor',authors)
    },
    async addAuthor({commit}, data) {
        const authors = await AuthorService.postAuthor(data.name);
        commit('addUser',authors)
    }
}

const mutations = {
    setAuthor(state,authors) {
        if(authors) {
            state.authors = authors.data
            state.total = authors.total
        }
    },
    addUser(state,authors) {
        if(authors) {
            state.authors.push(authors)
            state.total = this.total + 1
        } 
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}