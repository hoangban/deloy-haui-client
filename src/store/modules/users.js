import user from "../../service/userService"

const state = ()=> ({
    user : [],
    detailUser:[]
})

const getters = {}

const mutations = {
    setUsers(state,users) {
        if(users) state.user = users
    },
    addUser(state,user) {
        if(user) state.user.push(user)
    },
    setDetailUsers(state,user){
        if(user) state.detailUser = user.data
    },
    updateUsers(state,data){
        if(data){
            state.detailUser.school = data.school,
            state.detailUser.faculty_id = data.faculty_id,
            state.detailUser.majors_id = data.majors_id,
            state.detailUser.tel = data.tel,
            state.detailUser.birthday = data.birthday,
            state.detailUser.firstname = data.firstname,
            state.detailUser.lastname = data.lastname
        } 
    }
}

const actions = {
    async getAllUser({commit}) {
       const users = await user.getListUser();
       if(users) commit('setUsers',users)
    },
    addUser({commit},name) {
        const data = {
            "id":3,
            "name": name,
            "email":"s@mail.com",
            "gender":"Male",
            "status":"Inactive",
            "created_at":"2020-09-25T03:50:03.651+05:30",
            "updated_at":"2020-09-25T09:45:45.881+05:30"}
        commit('addUser',data)
    },
    async getDetailUser({commit}){
        const detailUsers = await user.getDetailUser();
       if(detailUsers) commit('setDetailUsers',detailUsers)
    },
    async postEditUser({commit},data){
        const editUsers = await user.updateUser(data);
       if(editUsers) commit('updateUsers',data)
    }
}


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}