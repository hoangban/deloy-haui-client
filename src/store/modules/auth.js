import AuthService from "../../service/AuthService";

const state = ()=> ({
    user : []
})

// getters
const getters = {}

const actions = {
    async postLogin({commit}, data ) {
        const user = await AuthService.login(data.username, data.password,data.remember_me);
        if(user.access_token) {
            localStorage.setItem('user', JSON.stringify(user));
        }
        if(user) {
            commit('loginSuccess',user) 
        }
    },
    logout() {
        // xoá user từ local storage để log out
        localStorage.removeItem('user');
    }
};

const mutations = {
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = user;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }