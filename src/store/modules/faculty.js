import FacultyService from "../../service/FacultyService"

const state = ()=> ({
    facultys : [],
    total :''
})
const getters = {}

const actions = {
    async getAllFaculty({commit},data) {
        const facultys = await FacultyService.getListFaculty(data.perPage,data.currentPage);
        if(facultys) commit('setFaculty',facultys)
    },
    async addFaculty({commit}, data) {
        const facultys = await FacultyService.postFaculty(data.name,data.code);
        commit('addUser',facultys)
    }
}
const mutations = {
    setFaculty(state,facultys) {
        if(facultys) {
            state.facultys = facultys.data
            state.total = facultys.total
        }
    },
    addUser(state,facultys) {
        if(facultys) {
            state.facultys.push(facultys)
            state.total = this.total + 1
        } 
    }
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}