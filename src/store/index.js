import Vue from 'vue'
import Vuex from 'vuex'
import document from './modules/document'
import auth from './modules/auth'
import users from './modules/users'
import author from './modules/author'
import faculty from './modules/faculty'
import recomment from './modules/recomment'
import major from './modules/major'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    document,
    users,
    author,
    faculty,
    recomment,
    major
  }
})
